import random


class Card(object):
    """Card class to represent a single card in a deck of cards."""

    SUITS = {"C": "Clubs", "D": "Diamonds", "H": "Hearts", "S": "Spades"}
    VALUES = (2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14)

    def __init__(
        self,
        suit: str,
        value: int,
    ) -> None:
        """Initialize a card with a suit and value."""
        self.value = value
        self.suit = suit

    def __str__(self) -> str:
        """Return a string representation of the card."""
        if self.value == 14:
            value = "A"
        elif self.value == 13:
            value = "K"
        elif self.value == 12:
            value = "Q"
        elif self.value == 11:
            value = "J"
        else:
            value = self.value
        return f"{value} of {self.SUITS[self.suit]}"

    def __repr__(self):
        """Return a string representation of the card for the terminal."""
        return self.__str__()

    def __eq__(self, other) -> bool:
        """Check if two cards are equal."""
        return self.value == other.value

    def __ne__(self, other) -> bool:
        """Check if two cards are not equal."""
        return self.value != other.value

    def __lt__(self, other) -> bool:
        """Check if one card is less than another."""
        return self.value < other.value

    def __le__(self, other) -> bool:
        """Check if one card is less than or equal to another."""
        return self.value <= other.value

    def __gt__(self, other) -> bool:
        """Check if one card is greater than another."""
        return self.value > other.value

    def __ge__(self, other) -> bool:
        """Check if one card is greater than or equal to another."""
        return self.value >= other.value


class Deck:
    def __init__(self) -> None:
        """Initialize a deck of cards with 52 cards."""
        self.deck = []
        for suit in Card.SUITS:
            for value in Card.VALUES:
                card = Card(suit, value)
                self.deck.append(card)

    def shuffle(self) -> None:
        """
        "shuffles" the deck based on the Fisher-Yates algorithm.

        notes:
        - starts from end to beginning
        - each iteration a random number is picked in order to swap the current card with another card
        """
        deck_length = len(self.deck)
        for index in range(deck_length - 1, 0, -1):
            rand_int = random.randint(0, index)
            self.deck[index], self.deck[rand_int] = (
                self.deck[rand_int],
                self.deck[index],
            )

    def __len__(self) -> int:
        """Return the number of cards left in the deck."""
        return len(self.deck)

    def deal_one_card(self) -> Card | None:
        """Deal one card from the deck. Return card from the top or None if the deck is empty."""
        if len(self) == 0:
            return None
        else:
            # 0 is the top of the deck
            return self.deck.pop(0)


class Poker:
    """Poker class to represent a game of poker (or at least the start of one)."""

    def __init__(self: object, num_hands: int = 1) -> None:
        """
        Initialize a game of poker with a deck of cards that is shuffled.
        Leave the unused parameter num_hands for future expansion, as well as to
        spur conversation about ways to expand the game/assesment.
        """
        self.deck = Deck()
        self.deck.shuffle()

    def play(self) -> Card | None:
        """Deal a card from the deck to the player/user."""
        return self.deck.deal_one_card()


def main():
    """Play a game of poker and print the card that was dealt."""
    game = Poker()
    card = game.play()
    print("Your Card:", card)


if __name__ == "__main__":
    main()
