# Poker Assessment
This is an assessment for Strider.

The objective is to create a set of Python classes that represent a deck of poker-style playing
cards (fifty-two playing cards in four suits: hearts, spades, clubs, diamonds, with face values of
Ace, 2-10, Jack, Queen, and King).

Within one of your classes, you must provide two operations: 

1. shuffle()
Shuffle returns no value, but results in the cards in the deck being randomly permuted. Please
do not use library-provided shuffle operations to implement this function. You may use library provided random number generators in your solution if needed. 

2. deal_one_card()
This function should return the card from the “top” of the deck to the caller. If the deck is empty,
no card is dealt. 

## How to Run the Script
The script is meant to be easily ran by calling `python` along with the module name:
```bash
python poker.py
```

## How to Run Tests
To run tests you just need to call `python` along with the test module:
```bash
python tests.py
```

### Notes on Testing
The tests exist to test each aspect of the `poker.py` module. I wanted to make sure each class functioned exactly as expected.
The tests also were a quick way to test for each of the criteria desired by the assessment. In example: `test_deck_deal_end_of_deck` allows for an easy way to 
confirm `None`, a representation that No card is dealt at the end of the deck when all 52 cards are "dealt".

## Notes on Development
During the development I wanted to begin with following strict guidelines and excellent formatting. So for this I used the following:
- `pre-commit`: I like using pre-commit often to help enforce readable code.
- `isort`: for sorting imports and helping to ensure they are well organized. _(this is mainly from my personal default pre-commit-config not really needed for this project)_
- `black`: I'm not picky on formatters but lately I find myself using `black` more. The more modern character length isn't such a bad thing and I think `"` for stings allow for more flexibility.

Through the code it should be noticed type hinting is used on all functions as well as args. This is a personal choice I like to add when developing as it helps with ide's detect attributes of objects, as well as writing maintainable code.

During development I started out with the `Card` class first because it would be the obvious first choice to start. All cards at the end of the day despite `SUIT` would be a card. A `SUIT` and `VALUE` of a card would just be attributes of a card. Then a `Deck` would be used to hold many cards as well as the state of all the cards. It would ideally house the functions needed for the assessement as they would mainly effect the state of the deck. Creating the `Poker` class is more so to show the utilization of the previous classes while also helping to note the ability to reuse the classes in other types of games.