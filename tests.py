import unittest
from poker import Poker, Card, Deck


class TestCards(unittest.TestCase):
    """Test Card class creation and comparison."""

    def test_card_creation(self):
        """Test cards are able to be created with different suits and values."""
        three_of_hearts = Card("H", 3)
        self.assertEqual(str(three_of_hearts), "3 of Hearts")

        king_of_spades = Card("S", 13)
        self.assertEqual(str(king_of_spades), "K of Spades")

    def test_card_comparison(self):
        """Test that cards are created by comparing against their values."""
        three_of_hearts = Card("H", 3)
        king_of_spades = Card("S", 13)
        self.assertTrue(three_of_hearts < king_of_spades)
        self.assertTrue(king_of_spades > three_of_hearts)
        self.assertTrue(three_of_hearts != king_of_spades)
        self.assertTrue(three_of_hearts == three_of_hearts)


class TestDeck(unittest.TestCase):
    """Test Deck class creation and shuffle method."""

    def test_deck_creation(self):
        """Test that a deck is created with 52 cards."""
        deck = Deck()
        self.assertEqual(len(deck.deck), 52)

    def test_deck_shuffle(self):
        """Test that a deck is shuffled and not in the original order."""
        deck = Deck()
        # acts as a copy of the original deck instead of a reference
        original_deck = deck.deck.copy()
        deck.shuffle()
        self.assertNotEqual(original_deck, deck.deck)

    def test_deck_deal(self):
        """Test that a card is dealt from the deck and the deck is reduced by one."""
        deck = Deck()
        card = deck.deal_one_card()
        self.assertEqual(len(deck.deck), 51)
        self.assertIsInstance(card, Card)

    def test_deck_deal_end_of_deck(self):
        """Test that when the deck is empty, None is returned."""
        deck = Deck()
        for index in range(52):
            deck.deal_one_card()
        card = deck.deal_one_card()
        self.assertIsNone(card)


class TestPoker(unittest.TestCase):
    """Test Poker class methods."""

    def test_poker_creation(self):
        """Test that a Poker class is created and deals a card to users."""
        poker = Poker()
        self.assertEqual(len(poker.deck.deck), 52)
        poker.play()
        self.assertEqual(len(poker.deck.deck), 51)


if __name__ == "__main__":
    unittest.main()
